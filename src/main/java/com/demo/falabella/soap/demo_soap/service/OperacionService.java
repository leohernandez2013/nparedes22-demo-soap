package com.demo.falabella.soap.demo_soap.service;

import com.demo.falabella.soap.demo_soap.models.Operacion;

public interface OperacionService {
	public Operacion ejecutarOperacion(Operacion operacion);
	public Boolean guardarOperacion(Operacion operacion);
}
