package com.demo.falabella.soap.demo_soap.dao.impl;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.demo.falabella.soap.demo_soap.dao.OperacionDao;
import com.demo.falabella.soap.demo_soap.models.Operacion;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class OperacionDaoImpl implements OperacionDao {

	@Override
	public Boolean saveOperacion(Operacion operacion) {
		Boolean resultado = false;
		@SuppressWarnings("squid:S2068")
		String connection_string = "jdbc:db2://localhost:50001/testdb:" + "user=db2inst1;password=ACL.2019;";
		URL url_query = Resources.getResource("insert.sql");
		Connection conn = null;
		PreparedStatement prst = null;
		try {
			String query = Resources.toString(url_query, Charsets.UTF_8);
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			conn = DriverManager.getConnection(connection_string);
			prst = conn.prepareStatement(query);
			prst.setString(1, operacion.getOperacion().name().toLowerCase());
			prst.setInt(2, operacion.getValor1());
			prst.setInt(3, operacion.getValor2());
			prst.setInt(4, operacion.getResultado());
			int rs = prst.executeUpdate();
			if (rs != 0) {
				resultado = true;
			}
		} catch (SQLException | IOException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			} finally {
				try {
					if (prst != null) {
						prst.close();
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
		return resultado;
	}

}
