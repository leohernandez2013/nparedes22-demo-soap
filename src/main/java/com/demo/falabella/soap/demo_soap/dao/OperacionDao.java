package com.demo.falabella.soap.demo_soap.dao;

import com.demo.falabella.soap.demo_soap.models.Operacion;

public interface OperacionDao {

	public Boolean saveOperacion(Operacion operacion);
}
